# Configuration

## Themes

```
;; A low contrast color theme for Emacs
(use-package zenburn-theme
  :pin melpa
  :config
  (setq zenburn-scale-org-headlines t
        zenburn-use-variable-pitch t
        zenburn-scale-outline-headlines t)
  (load-theme 'zenburn t)
  (set-cursor-color "#D0BF8F"))
```

## Fonts

```
(set-face-attribute 'default nil :family "DejaVu Sans Mono" :height 130 :weight 'book)
(set-face-attribute 'variable-pitch nil :family "DejaVu Serif" :height 130 :weight 'book)
(set-face-attribute 'fixed-pitch nil :family "DejaVu Sans Mono" :height 130 :weight 'book)

(set-face-attribute 'default nil :family "Source Code Pro" :height 130 :weight 'regular)
(set-face-attribute 'variable-pitch nil :family "Cantarell" :height 130 :weight 'regular)
(set-face-attribute 'fixed-pitch nil :family "Source Code Pro" :height 130 :weight 'regular)
```
